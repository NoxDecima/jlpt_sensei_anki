import os

import genanki
import requests
from bs4 import BeautifulSoup
from tqdm import tqdm
import romkan


def readFile(file):
    with open(file, "r") as f:
        return f.read()


jlpt_grammer_model = genanki.Model(
    2006830367,
    'JLPT Grammar',
    fields=[
        {'name': 'Grammar'},
        {'name': 'Furigana'},
        {'name': 'Meaning'},
        {'name': 'Usage'},
        {'name': 'Level'},
        {'name': 'Example 1 Kanji'},
        {'name': 'Example 1 Furigana'},
        {'name': 'Example 1 English'},
        {'name': 'Example 2 Kanji'},
        {'name': 'Example 2 Furigana'},
        {'name': 'Example 2 English'},
        {'name': 'Example 3 Kanji'},
        {'name': 'Example 3 Furigana'},
        {'name': 'Example 3 English'}
    ],
    templates=[
        {
            'name': 'JLPT Grammar',
            'qfmt': readFile("./model/grammar/front.html"),
            'afmt': readFile("./model/grammar/back.html")
        }
    ],
    css=readFile("./model/grammar/style.css")
)


def grab_urls(url):
    urls = []

    while url is not None:
        page = requests.get(url)

        soup = BeautifulSoup(page.content, "html.parser")

        rows = soup.find_all(class_="jl-row")

        for row in rows:
            urls.append(row.find("a").get("href"))

        next = soup.find(class_="next")

        if next:
            url = next.get("href")
        else:
            url = None

    return urls


def make_note(url):
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
                             'Chrome/56.0.2924.76 Safari/537.36'}
    page = requests.get(url, headers=headers)

    if page.status_code == 200:
        soup = BeautifulSoup(page.content, "html.parser")

        grammar = soup.find(class_="jp").text
        furigana = romkan.to_hiragana(
            soup.find(class_="jp").parent.contents[1].replace("(", "").replace(")", "").replace(" ", ""))
        meaning = soup.find(id="meaning").find(class_='eng-definition').text
        usage = soup.find(id="usage").find("tbody").parent

        jlpt_level = soup.find(class_="glm-level").find("a").text

        tags = [jlpt_level]
        tags = [tag.replace(" ", "_") for tag in tags]

        examples = []
        for i in range(3):
            example = soup.find(id=f"example_{i + 1}")

            if example is not None:
                kanji = example.find(class_='m-0 jp')
                reading = example.find(id=f"example_{i + 1}_ja").text
                english = example.find(id=f"example_{i + 1}_en").text

                examples.append((str(kanji), reading, english))
            else:
                examples.append(("", "", ""))

        return genanki.Note(
            model=jlpt_grammer_model,
            fields=[str(grammar), furigana, meaning, str(usage), jlpt_level,
                    examples[0][0], examples[0][1], examples[0][2],
                    examples[1][0], examples[1][1], examples[1][2],
                    examples[2][0], examples[2][1], examples[2][2],
                    ],
            tags=tags
        )
    else:
        print(url)
        return None


def save_deck(deck, file):
    genanki.Package(deck).write_to_file(file)


if __name__ == '__main__':
    os.makedirs("decks/grammar", exist_ok=True)

    for i in range(5):
        deck = genanki.Deck(
            1262645900 + i,
            f"N{i + 1}"
        )

        urls = grab_urls(f"https://jlptsensei.com/jlpt-n{i + 1}-grammar-list/")

        for url in tqdm(urls, desc=f"N{i + 1}"):
            note = make_note(url)
            if note is not None:
                deck.add_note(note)

        save_deck(deck, f'decks/grammar/N{i + 1}.apkg')

