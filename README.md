# Anki JLPT deck webscraper

This project contains code that scrapes code from https://jlptsensei.com to create anki decks for learning Japanese. Decks are seperated by JLPT Level.

Currently includes sripts to create the following decks:
1. JLPT Grammer (Levels N5 to N1) 